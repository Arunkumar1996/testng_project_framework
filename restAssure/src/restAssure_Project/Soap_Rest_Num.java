package restAssure_Project;

import org.testng.Assert;
import io.restassured.path.xml.XmlPath;
import static io.restassured.RestAssured.given;
import io.restassured.RestAssured;

public class Soap_Rest_Num {
	public static void main(String[] args) {
		//step1: Declare a Base URL
String BaseURI="https://www.dataaccess.com";
//step2 Declare the request body
		String requestBody="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://www.dataaccess.com/webservicesserver/\">\r\n"
				+ "   <soapenv:Header/>\r\n"
				+ "   <soapenv:Body>\r\n"
				+ "      <web:NumberToDollars>\r\n"
				+ "         <web:dNum>55</web:dNum>\r\n"
				+ "      </web:NumberToDollars>\r\n"
				+ "   </soapenv:Body>\r\n"
				+ "</soapenv:Envelope>";
		    
		  //step3 Trigger the API  and fetch the Response body
			//RestAssured.baseURI=BaseURI;
			String responseBody=given().header("Content-Type", "text/xml; charset=utf-8").body(requestBody).when().post("https://www.dataaccess.com/webservicesserver/NumberConversion.wso")
			.then().extract().response().getBody().asString();
			
			//Step4 print the response body
			System.out.println(responseBody);
			
			//step5 extract the response body parameter
			XmlPath Xml_res=new XmlPath(responseBody);
			String res_tag=Xml_res.getString("NumberToDollarsResult");
			System.out.println(res_tag);
			
			//step6 Validate the response body
			Assert.assertEquals(res_tag, "fifty five dollars");
	}

}
