package practice_Api;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import static io.restassured.RestAssured.given;
import java.time.LocalDateTime;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Post {
	@Test
	public static void post_execution(){
	  	RestAssured.baseURI="https://reqres.in/";
	  	String requestBody="{\r\n"
	  			+ "    \"name\": \"morpheus\",\r\n"
	  			+ "    \"job\": \"leader\"\r\n"
	  			+ "}";
	  	String responseBody =given().header("Content-Type","application/json").body(requestBody).when().post("api/users").then().extract().asString();
	  	System.out.println(responseBody);
	  	JsonPath jsp_req=new JsonPath(requestBody);
	  	String  req_name=jsp_req.getString("name");
		String  req_job=jsp_req.getString("job");
		LocalDateTime currentdate=LocalDateTime.now();
		String exp_date =currentdate.toString().substring(0,11);
		JsonPath jsp_res=new JsonPath(responseBody);
		String res_name=jsp_res.getString("name");
		String res_job =jsp_res.getString("job");
		String res_id=jsp_res.getString("id");
		String res_createdAt=jsp_res.getString("createdAt").substring(0,11);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdAt,exp_date);
		
			
		
		
	  	
	}

}
