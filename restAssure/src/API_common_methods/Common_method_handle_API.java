package API_common_methods;

import static io.restassured.RestAssured.given;

public class Common_method_handle_API {

	public static int Post_statuscode(String requestBody, String endpoint) {
		int statusCode = given().header("Content-Type", "application/json").body(requestBody).when().post(endpoint)
				.then().extract().statusCode();
		return statusCode;
	}

	public static String Post_responseBody(String requestBody, String endpoint) {
		String responseBody = given().header("Content-Type", "application/json").body(requestBody).when().post(endpoint)
				.then().extract().response().asString();
		return responseBody;

	}

	public static int Put_statuscode(String requestBody, String endpoint) {
		int statusCode = given().header("Content-Type", "application/json").body(requestBody).when().put(endpoint)
				.then().extract().statusCode();
		return statusCode;
	}

	public static String Put_responseBody(String requestBody, String endpoint) {
		String responseBody = given().header("Content-Type", "application/json").body(requestBody).when().put(endpoint)
				.then().extract().response().asString();
		return responseBody;

	}

	public static int Patch_statuscode(String requestBody, String endpoint) {
		int statusCode = given().header("Content-Type", "application/json").body(requestBody).when().patch(endpoint)
				.then().extract().statusCode();
		return statusCode;
	}

	public static String Patch_responseBody(String requestBody, String endpoint) {
		String responseBody = given().header("Content-Type", "application/json").body(requestBody).when()
				.patch(endpoint).then().extract().response().asString();
		return responseBody;
	}

	public static int get_statuscode( String endpoint) {
		int statusCode = given().header("Content-Type", "application/json").when().get(endpoint).then().extract()
				.statusCode();
		return statusCode;
	}

	public static String get_responseBody(String endpoint) {
		String responseBody = given().header("Content-Type", "application/json").when().get(endpoint).then().extract()
				.response().asString();
		return responseBody;
	}
	public static int del_statuscode( String endpoint) {
		int statusCode = given().header("Content-Type", "application/json").when().delete(endpoint).then().extract()
				.statusCode();
		return statusCode;
	}

}