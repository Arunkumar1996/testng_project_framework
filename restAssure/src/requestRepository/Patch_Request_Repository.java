package requestRepository;

import java.io.IOException;
import java.util.ArrayList;

import Utility_Common_method.Data_Extractor_Excel;

public class Patch_Request_Repository {
	public static String Patch_request_TC1() throws IOException {
		ArrayList<String> Data = Data_Extractor_Excel.Data_Extractor_reader("Test_data", "patch_api", "patch_TC5");
		String name = Data.get(1);
		String job = Data.get(2);
		String Patch_requestbody ="{\r\n"+ "    \"name\": \""+name+"\",\r\n"+ "    \"job\": \""+job+"\"\r\n"+ "}"; 
		return Patch_requestbody;
	}
	

}
