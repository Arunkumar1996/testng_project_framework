package Test_package;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import API_common_methods.Common_method_handle_API;
import Endpoint.Get_Endpoint;
import Utility_Common_method.Handle_api_logs;
import Utility_Common_method.Handle_directory;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class get_TC1 extends Common_method_handle_API {
      static File log_dir;
      static String get_endpoint;
      static String get_responseBody;
	
     @BeforeTest
	public static void Testsetup() {
		  log_dir = Handle_directory.create_log_directory("get_TC1_logs");
			 get_endpoint=Get_Endpoint.Get_Endpoint_Tc1();
		 get_responseBody = get_responseBody(get_endpoint);
	}
	@Test(description ="::::::Executing The PATCH API And Validating responseBody::::::")
	public static void get_executor() throws IOException {

		//String get_endpoint = "https://reqres.in/api/users?page=2";
		for (int i = 0; i < 5; i++) {
			int get_statusCode = get_statuscode(get_endpoint);
			System.out.println(get_statusCode);
			if (get_statusCode == 200) {
				
				System.out.println(get_responseBody);
				get_TC1.validator(get_responseBody);
				break;
			} else {
				System.out.println("expected statuscode is not found hence retrying");
			}

		}
	}

	public static void validator(String get_responseBody) {
		int expected_id[] = { 7, 8, 9, 10, 11, 12 };
		String expected_firstname[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String expected_lastname[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
		String expected_email[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		

		
		JSONObject array_res = new JSONObject(get_responseBody);
		JSONArray dataarray = array_res.getJSONArray("data");
		
		int count = dataarray.length();
		for (int i = 0; i < count; i++) {
			
			int res_id = dataarray.getJSONObject(i).getInt("id");
			String res_firstname = dataarray.getJSONObject(i).getString("first_name");
			String res_lastname = dataarray.getJSONObject(i).getString("last_name");
			String res_email = dataarray.getJSONObject(i).getString("email");
			
			int exp_id = expected_id[i];
			String exp_firstname = expected_firstname[i];
			String exp_lastname = expected_lastname[i];
			String exp_emial = expected_email[i];
		
			Assert.assertEquals(res_id, exp_id);
			Assert.assertEquals(res_firstname, exp_firstname);
			Assert.assertEquals(res_lastname, exp_lastname);
			Assert.assertEquals(res_email, exp_emial);

		}
	}
	@AfterTest
	public static void Teardown() throws IOException {
		Handle_api_logs.evidence_creator(log_dir,"get_TC1", get_endpoint,  get_responseBody);
	}
}