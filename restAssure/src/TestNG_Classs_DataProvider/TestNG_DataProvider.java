package TestNG_Classs_DataProvider;

import org.testng.annotations.DataProvider;

public class TestNG_DataProvider {
	@DataProvider()
	public Object[][] post_data_provider(){
		return new Object[][]
				{
					{"ARUN","QA"},
					{"AMAN","DEV"},
					{"AJITH","SRQA"}
				};
	}
	@DataProvider()
	public Object[][] put_data_provider(){
		return new Object[][]
				{
					{"ARUN","QA"},
					{"ANUSHA","SRDEV"},
					{"ANKITHA","SRQA"}
				};
	}
	
	@DataProvider()
	public Object[][] patch_data_provider(){
		return new Object[][]
				{
					{"ARUN","QA"},
					{"ANU","JRDEV"},
					{"ANKIT","SRQA"}
				};
	}
}

