package TestNG_DifferentClass_Dataprovider;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import API_common_methods.Common_method_handle_API;
import Endpoint.Patch_Endpoint;
import TestNG_Classs_DataProvider.TestNG_DataProvider;
import Utility_Common_method.Handle_api_logs;
import Utility_Common_method.Handle_directory;
import io.restassured.path.json.JsonPath;

public class TestNG_DataProvider_Patch {
	public class patch_TC1 extends Common_method_handle_API {
		static File log_dir;
		static String patch_requestBody;
		static String patch_endpoint;
		static String patch_responseBody;
		@DataProvider()
		public Object[][] patch_requestBody(){
			return new Object[][]
					{
						{"morpheus","leader"},
						{"Arunkumar","QA"},
						{"Sai","Hr"}
					};
		}
		
		@BeforeTest
		public static void Testsetup() throws IOException {
			 log_dir = Handle_directory.create_log_directory("patch_TC1_logs");
				 patch_endpoint=Patch_Endpoint.Patch_Endpoint_Tc1();
			
		}
		
		//@Test(dataProvider  ="patch_requestBody")
		@Test(dataProvider = "patch_data_provider",dataProviderClass = TestNG_DataProvider.class)
		public static void patch_executor(String name,String job) throws IOException {
			
			for (int i = 0; i < 5; i++) {
				 patch_requestBody="{\r\n"
					 		+ "    \"name\": \""+name+"\",\r\n"
					 		+ "    \"job\": \""+job+"\"\r\n"
					 		+ "}";
				int patch_statusCode = Patch_statuscode(patch_requestBody, patch_endpoint);
				System.out.println(patch_statusCode);
				if (patch_statusCode == 200) {
					patch_responseBody = Patch_responseBody(patch_requestBody, patch_endpoint);
					System.out.println(patch_responseBody);
					Handle_api_logs.evidence_creator(log_dir,"patch_TC1", patch_endpoint, patch_requestBody, patch_responseBody);
					patch_TC1.validator(patch_requestBody, patch_responseBody);
					break;
				} else {
					System.out.println("expected statuscode is not found hence retrying");
				}
			}
		}
		public static void validator(String requestBody,String responseBody) {
			JsonPath jsp_req = new JsonPath(requestBody);
			String req_name = jsp_req.getString("name");
			String req_job = jsp_req.getString("job");
			LocalDateTime currentdate = LocalDateTime.now();
			String expecteddate = currentdate.toString().substring(0, 11);
			JsonPath jsp_res = new JsonPath(responseBody);
			String res_name = jsp_res.getString("name");
			String res_job = jsp_res.getString("job");
			String res_updatedAt = jsp_res.getString("updatedAt");
			res_updatedAt = res_updatedAt.substring(0, 11);
			Assert.assertEquals(res_name, req_name);
			Assert.assertEquals(res_job, req_job);
			Assert.assertEquals(res_updatedAt, expecteddate);
		}
		
		@AfterTest
		public static void TestTeardown() throws IOException {
			String Testname =patch_TC1.class.getName();
			Handle_api_logs.evidence_creator(log_dir,Testname, patch_endpoint, patch_requestBody, patch_responseBody);
			
		}
			
		}

}
