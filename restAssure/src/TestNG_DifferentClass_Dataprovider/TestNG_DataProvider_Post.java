package TestNG_DifferentClass_Dataprovider;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import API_common_methods.Common_method_handle_API;
import Endpoint.Post_Endpoint;
import TestNG_Classs_DataProvider.TestNG_DataProvider;
import Utility_Common_method.Handle_api_logs;
import Utility_Common_method.Handle_directory;
import io.restassured.path.json.JsonPath;

public class TestNG_DataProvider_Post extends Common_method_handle_API {
	static File log_dir;
	static String requestBody;
	static String Endpoint;
	static String responseBody;
	
	@DataProvider()
	public Object[][] post_requestBody(){
		return new Object[][]
				{
					{"morpheus","leader"},
					{"Arunkumar","QA"},
					{"Sai","Hr"}
				};
	}
	
	@BeforeTest
	public static void Testsetup() throws IOException {
		 log_dir = Handle_directory.create_log_directory("post_TC1_logs");
			 Endpoint = Post_Endpoint.Post_Endpoint_Tc1();
	}
   // @Test(dataProvider ="post_requestBody")
	@Test(dataProvider = "post_data_provider",dataProviderClass = TestNG_DataProvider.class)
	public static void post_executor(String name,String job) throws IOException {
			 requestBody ="{\r\n"
			 		+ "    \"name\": \""+name+"\",\r\n"
			 		+ "    \"job\": \""+job+"\"\r\n"
			 		+ "}";
		for (int i = 0; i < 5; i++) {
			int statusCode = Post_statuscode(requestBody, Endpoint);
			System.out.println(statusCode);
			if (statusCode == 201) {
				responseBody = Post_responseBody(requestBody, Endpoint);
				System.out.println(responseBody);
				Handle_api_logs.evidence_creator(log_dir,"post_TC1", Endpoint, requestBody, responseBody);
				TestNG_DataProvider_Post.validator(requestBody, responseBody);
				break;
			} else {
				System.out.println("expected statuscode is not found hence retrying");
			}
		}
	}

	public static void validator(String requestbody, String responsebody) {
		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0,11);
		JsonPath jsp_res = new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_id = jsp_res.getString("id");
		String res_createdAt = jsp_res.getString("createdAt");
		res_createdAt = res_createdAt.substring(0,11);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdAt, expecteddate);
	}
	
	@AfterTest
	public static void TestTeardown() throws IOException {
		String testname=TestNG_DataProvider.class.getName();
		Handle_api_logs.evidence_creator(log_dir,testname, Endpoint, requestBody, responseBody);
		
	}
}


