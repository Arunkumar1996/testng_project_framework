package TestNG_DifferentClass_Dataprovider;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import API_common_methods.Common_method_handle_API;
import Endpoint.Put_Endpoint;
import TestNG_Classs_DataProvider.TestNG_DataProvider;
import Test_package.put_TC1;
import Utility_Common_method.Handle_api_logs;
import Utility_Common_method.Handle_directory;
import io.restassured.path.json.JsonPath;
import requestRepository.Put_Request_Repository;

public class TestNG_DataProvider_Put extends Common_method_handle_API {
	static File log_dir;
	static 	String put_requestBody;
	static  String put_endpoint;
	static  String put_responseBody;
	@DataProvider()
	public Object[][] put_requestBody(){
		return new Object[][]
				{
					{"morpheus","leader"},
					{"Arunkumar","QA"},
					{"Sai","Hr"}
				};
	}
	
	@BeforeTest
	public static void Testsetup() throws IOException {
		log_dir = Handle_directory.create_log_directory("put_TC1_logs");
			
			put_endpoint=Put_Endpoint.Put_Enpoint_Tc1();
		 
	}
	//@Test(dataProvider = "put_requestBody")
	@Test(dataProvider = "put_data_provider",dataProviderClass = TestNG_DataProvider.class)
	public static void put_executor(String name,String job) throws IOException {
		
		for (int i = 0; i < 5; i++) {
			 put_requestBody="{\r\n"
				 		+ "    \"name\": \""+name+"\",\r\n"
				 		+ "    \"job\": \""+job+"\"\r\n"
				 		+ "}";
			int put_statusCode = Put_statuscode(put_requestBody, put_endpoint);
			System.out.println(put_statusCode);
			if (put_statusCode == 200) {
				String put_responseBody = Put_responseBody(put_requestBody, put_endpoint);
				System.out.println(put_responseBody);
				
				put_TC1.validator(put_requestBody, put_responseBody);
				break;
			} else {
				System.out.println("expected statuscode is not found hence retrying");
			}
		}
	}
	
	
	public static void validator(String requestbody, String responsebody) {
		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);
		JsonPath jsp_res = new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_updatedAt = jsp_res.getString("updatedAt");
		res_updatedAt = res_updatedAt.substring(0, 11);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_updatedAt, expecteddate);
		
	}
	@AfterTest
	public static void TestTeardown() throws IOException {
		String Testname = put_TC1.class.getName();
		Handle_api_logs.evidence_creator(log_dir,Testname, put_endpoint, put_requestBody, put_responseBody);
		
		
	}
}


